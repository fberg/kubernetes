#!/bin/bash
#
# Configures instances with the basic requirements for a kubernetes
# installation.
#   Parameters (provided my mom-template.yaml):
#     $KUBERNETES_VERSION  version or Kubernetes to install
#     $CONTAINERD_VERSION  version of containerd to install
#  Result:
#     Node and containerd are configured to support kubernetes. Kubernetes is
#     installed and locked to desired major version.
apt-get update
# --- networking ---
cat <<___HERE >> /etc/modules-load.d/k8s.conf
overlay
br_netfilter
___HERE
modprobe overlay
modprobe br_netfilter
# sysctl params required by setup, params persist across reboots
cat <<___HERE >> /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
___HERE
sysctl --system

# --- container runtime ---
apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
  https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
  >> /etc/apt/sources.list.d/docker.list
apt-get update
apt-get install -y "containerd.io=$CONTAINERD_VERSION"'.*'
apt-mark hold containerd.io

# --- Container network
wget https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-amd64-v1.2.0.tgz
mkdir -p /opt/cni/bin
tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.2.0.tgz
mkdir -p /etc/cni/net.d
cat <<___HERE > /etc/cni/net.d/10-containerd-net.conflist
{
  "cniVersion": "1.0.0",
  "name": "containerd-net",
  "plugins": [
    {
      "type": "bridge",
      "bridge": "cni0",
      "isGateway": true,
      "ipMasq": true,
      "promiscMode": true,
      "ipam": {
        "type": "host-local",
        "ranges": [
          [{
            "subnet": "10.88.0.0/16"
          }],
          [{
            "subnet": "2001:4860:4860::/64"
          }]
        ],
        "routes": [
          { "dst": "0.0.0.0/0" },
          { "dst": "::/0" }
        ]
      }
    },
    {
      "type": "portmap",
      "capabilities": {"portMappings": true}
    }
  ]
}
___HERE
# ---
sed -i '/^disabled_plugins/ s/"cri"// ' /etc/containerd/config.toml
# from https://github.com/kubernetes/kubeadm/issues/2767#issuecomment-1344620047
cat <<___HERE >> /etc/containerd/config.toml
version = 2
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
  runtime_type = "io.containerd.runc.v2"
  [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true
[plugins."io.containerd.grpc.v1.cri"]
  sandbox_image = "registry.k8s.io/pause:3.2"
___HERE
systemctl restart containerd

# --- kubeadm/kubelet/kubectl ---
apt-get update
apt-get install -y apt-transport-https ca-certificates curl
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg
echo \
    "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ \
    kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y \
    kubelet="$KUBERNETES_VERSION"'.*' \
    kubeadm="$KUBERNETES_VERSION"'.*' \
    kubectl="$KUBERNETES_VERSION"'.*'
apt-mark hold kubelet kubeadm kubectl
