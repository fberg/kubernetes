#!/bin/bash
#
# Configures the first kubernetes controller. Notifies stack wait conditions
# on completion.
#   Parameters:
#     $WORKER_KEY_PRIVATE:             ssh private key
#     $WORKER_KEY_PUBLIC:              ssh public key
#     $SUFFIX:                         unique string identifying the kubernetes
#     $CONTROLPLANE_IP:                IP to reach the Kubernetes API from outside
#     $APPLICATION_CREDENTIAL_ID:      OpenStack application credential ID
#     $APPLICATION_CREDENTIAL_SECRET:  Application credential secret
#     $SUBNET_ID:                      Kubernetes cluster subnet
#     $FLOATING_NETWORK_ID:            ID of network providing FIPs
#     $WC_NOTIFY:                      command to notify worker creation
#     $CONTROLLER_WC_NOTIFY:           command to notify controller creation
#     $KUBERNETES_VERSION:             Kubernetes version in use
#
echo "$WORKER_KEY_PRIVATE" > /root/.ssh/id_rsa
echo "$WORKER_KEY_PUBLIC" > /root/.ssh/id_rsa.pub
chmod 600 /root/.ssh/id_rsa

cat > /tmp/kubeadm.yaml <<EOF
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
nodeRegistration:
  kubeletExtraArgs:
    cloud-provider: "external"
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
clusterName: "$SUFFIX"
apiServer:
  extraArgs:
    cloud-provider: "external"
  certSANs:
    - "$CONTROLPLANE_IP"
controlPlaneEndpoint: "$CONTROLPLANE_IP:6443"
controllerManager:
  extraArgs:
    cloud-provider: "external"
networking:
  podSubnet: "10.244.0.0/16"
EOF

kubeadm init --upload-certs --config /tmp/kubeadm.yaml |tee /root/kubeinit.log
control_plane_join_cmd=$(grep -B2 '\-\-control-plane' /root/kubeinit.log | tr -d '\n\t\\\' | sed 's/^ *//')
certificate_key=$(grep '\-\-certificate-key' /root/kubeinit.log | sed 's/.*certificate-key //')
kubeadm init phase upload-certs --upload-certs --certificate-key "$certificate_key"
cp /etc/kubernetes/admin.conf /root/externaladmin.conf
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> /root/.bashrc
export KUBECONFIG=/etc/kubernetes/admin.conf
curl https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico-vxlan.yaml \
    | sed 's/CrossSubnet/Always/' \
    | kubectl apply -f -

cat > /tmp/cloud.conf <<EOF
[Global]
auth-url=https://hpccloud.mpcdf.mpg.de:13000/v3
region=regionOne
application-credential-id=$APPLICATION_CREDENTIAL_ID
application-credential-secret=$APPLICATION_CREDENTIAL_SECRET
[LoadBalancer]
subnet-id=$SUBNET_ID
floating-network-id=$FLOATING_NETWORK_ID
create-monitor=true
[BlockStorage]
ignore-volume-az=true
EOF

cat > /tmp/cinder.yaml <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: csi-sc-cinderplugin
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: cinder.csi.openstack.org
parameters:
  availability: nova
EOF

apt install -y git
git clone \
    --single-branch \
    --no-tags \
    --depth 1 \
    -b 'release-$KUBERNETES_VERSION' \
    https://github.com/kubernetes/cloud-provider-openstack.git \
    /tmp/cloud-provider-openstack
rm /tmp/cloud-provider-openstack/manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml

kubectl create secret -n kube-system generic cloud-config --from-file=/tmp/cloud.conf
kubectl apply -f /tmp/cloud-provider-openstack/manifests/controller-manager/cloud-controller-manager-roles.yaml
kubectl apply -f /tmp/cloud-provider-openstack/manifests/controller-manager/cloud-controller-manager-role-bindings.yaml
kubectl apply -f /tmp/cloud-provider-openstack/manifests/controller-manager/openstack-cloud-controller-manager-ds.yaml
kubectl apply -f /tmp/cloud-provider-openstack/manifests/cinder-csi-plugin
kubectl apply -f /tmp/cinder.yaml
$CONTROLLER_WC_NOTIFY --data-binary '{ "status": "SUCCESS", "data": "'"${control_plane_join_cmd}"'" }'
$WC_NOTIFY --data-binary '{ "status": "SUCCESS", "data": "'"$(kubeadm token create --print-join-command --ttl 0)"'" }'
