# Step-by-step installation of Kubernetes 1.25+ on the MPCDF HPC Cloud

The procedure below can be used to deploy a production-ready Kubernetes cluster
on the MPCDF [HPC Cloud](https://docs.mpcdf.mpg.de/doc/computing/cloud/),
including "out-of-the-box" support for persistent storage and load balancers.
The resulting cluster is intended to be functionally equivalent to the
[templatized version](../README.md).

*Values in `$ALL_CAPS` should be customized before running the command.*

*Instead of using the OpenStack CLI you can also use the
[dashboard](https://hpccloud.mpcdf.mpg.de/).*

## Variables
Set the following variables to your preferences:
```bash
EXTERNAL_NETWORK="cloud-public"
CONTROL_PLANE_FLAVOR="mpcdf.medium.ha"
KEYNAME="___"
CLUSTER_NAME="________" \
WORKER_FLAVOR="mpcdf.large"
NUM_WORKERS=3
KUBERNETES_VERSION=1.26
```
This is meant to ease the use of the instructions below. You can, of course,
enter values in place of using the variables below.

The flavors above are what is recommended from the flavors made avaialble by
default. If you need other hardware configurations to back your cluster, please
get in touch with us at the [helpdesk](https://helpdesk.mpcdf.mpg.de).

## Depoying the network


### Create a private network

You will need to request access to an external network such as *cloud-public*,
which is reachable from the internet. Be sure to use the same value in later
steps.
```bash
openstack network create k8s-net --mtu 1500
openstack subnet create k8s-subnet \
    --network k8s-net \
    --subnet-range 192.168.0.0/24 \
    --dns-nameserver 130.183.9.32 \
    --dns-nameserver 130.183.1.21
openstack router create k8s-router --external-gateway "${EXTERNAL_NETWORK}"
openstack router add subnet k8s-router k8s-subnet
```


### Security groups (i.e. Firewall rules)

Opening the traffic in our private network is safe as access is managed through
the load balancer and (optionally) the SSH  gateway.
```sh
openstack security group create k8s-secgroup
openstack security group rule create k8s-secgroup \
    --remote-ip 0.0.0.0/0 \
    --protocol icmp \
    --description "Allow all internal ICMP traffic"
openstack security group rule create k8s-secgroup \
    --remote-ip 0.0.0.0/0 \
    --protocol tcp \
    --description "Allow all internal TCP traffic"
```


### Create control-plane network ports

These network ports will be attached to the load balancer and the control plane
nodes. We select the IP addresses to make the rest of the steps easier to
follow:
```bash
openstack port create k8s-control-plane \
    --network k8s-net \
    --fixed-ip subnet=k8s-subnet,ip-address=192.168.0.3 \
    --security-group k8s-secgroup
openstack port create k8s-control-plane-0 \
    --network k8s-net \
    --fixed-ip subnet=k8s-subnet,ip-address=192.168.0.4 \
    --security-group k8s-secgroup
openstack port create k8s-control-plane-1 \
    --network k8s-net \
    --fixed-ip subnet=k8s-subnet,ip-address=192.168.0.5 \
    --security-group k8s-secgroup
openstack port create k8s-control-plane-2 \
    --network k8s-net \
    --fixed-ip subnet=k8s-subnet,ip-address=192.168.0.6 \
    --security-group k8s-secgroup
```


### Load balancer and netowrk ports for the control plane

Start by creating a load balancer:
```bash
openstack loadbalancer create \
    --name k8s-control-plane \
    --vip-port-id k8s-control-plane
```

We have to wait a minute for the load balancer to be provisioned: behind the
scenes there is a VM hosting an HA proxy being created for you. In the
meantime, we can assign a floating IP to the load balance, which will make the
kubernetes API reachable from the outside:
```bash
openstack floating ip create "${EXTERNAL_NETWORK}" \
    --description "${CLUSTER_NAME}" \
    --port k8s-control-plane
```
Remeber this IP addess of the DNS entry for the control plane configuration
later. *Note* that the description in the floating IP is optional. If you
provide it you get a DNS entry for your floating IP as:
`$CLUSTER_NAME.PROJECT_NAME.hpccloud.mpg.de`

Once the load balancer is up we can continue:
```bash
openstack loadbalancer listener create k8s-control-plane \
    --name k8s-control-plane-listener \
    --protocol TCP \
    --protocol-port 6443 \
    --allowed-cidr 192.168.0.0/24 \
    --allowed-cidr 130.183.0.0/16 \
    --allowed-cidr 10.0.0.0/8
openstack loadbalancer pool create \
    --name k8s-control-plane-pool \
    --lb-algorithm ROUND_ROBIN \
    --listener k8s-control-plane-listener \
    --protocol TCP
openstack loadbalancer healthmonitor create k8s-control-plane-pool \
    --name k8s-control-plane-healthmonitor \
    --delay 5 \
    --max-retries 4 \
    --timeout 10 \
    --type TCP
openstack loadbalancer member create k8s-control-plane-pool \
    --name k8s-control-plane-0 \
    --address 192.168.0.4 \
    --protocol-port 6443 
openstack loadbalancer member create k8s-control-plane-pool \
    --name k8s-control-plane-1 \
    --address 192.168.0.5 \
    --protocol-port 6443 
openstack loadbalancer member create k8s-control-plane-pool \
    --name k8s-control-plane-2 \
    --address 192.168.0.6 \
    --protocol-port 6443 
```


### Get SSH access to your cluster

#### Using the loadbalancer
This sets up the loadbalancer to distribute ssh traffic to the control plane
nodes. Once you are on a control plane node you can reach the rest of the
kubernetes cluster.
```bash
openstack loadbalancer listener create k8s-control-plane \
    --name ssh-control-plane-listener \
    --protocol TCP \
    --protocol-port 22 \
    --timeout-client-data 0 \
    --timeout-member-data 0 
openstack loadbalancer pool create \
    --name ssh-control-plane-pool \
    --lb-algorithm ROUND_ROBIN \
    --listener ssh-control-plane-listener \
    --protocol TCP \
    --session-persistence type=SOURCE_IP
openstack loadbalancer healthmonitor create ssh-control-plane-pool \
    --name ssh-control-plane-healthmonitor \
    --delay 5 \
    --max-retries 4 \
    --timeout 10 \
    --type TCP
openstack loadbalancer member create ssh-control-plane-pool \
    --name ssh-control-plane-0 \
    --address 192.168.0.4 \
    --protocol-port 22
openstack loadbalancer member create ssh-control-plane-pool \
    --name ssh-control-plane-1 \
    --address 192.168.0.5 \
    --protocol-port 22
openstack loadbalancer member create ssh-control-plane-pool \
    --name ssh-control-plane-2 \
    --address 192.168.0.6 \
    --protocol-port 22
```


## Bootstrapping kubernetes


### The first control plane node


#### Instantiation

Edit the script `../common-config.sh`. You will need to set the two variables
`CONTAINERD_VERSION` and `KUBERNETES_VERSION`, which are provided by HEAT when
using the template method. Then use the script as user data for the controller
and worker nodes you create:
```bash
openstack server create k8s-control-plane-0 \
    --image "Debian 11" \
    --flavor $CONTROL_PLANE_FLAVOR \
    --port k8s-control-plane-0 \
    --security-group k8s-secgroup \
    --key-name $KEYNAME \
    --user-data ../common-config.sh
```

Wait a moment for the node to come up and run the base configuration script.


#### Configuration

Use the provided `kubeadm.yaml` and fill in you *cluster name* and the *floating
IP or DNS name of the load balancer*. Copy the resulting configuration to the
control plane using scp.

The ssh into the control plane and initialize kubernetes. *Note:* that you will
land on a node based on your source IP. If you want to start configuring another
control plane node first, scp/ssh over to that one.
```bash
ssh ${CONTROL_PLANE_IP} -l root
kubeadm init --upload-certs --config kubeadm.yaml
```

You will be given a command to add control plane nodes to the cluster:

```bash
kubeadm join $CLUSTER_NAME:6443 --token ___ \
	--discovery-token-ca-cert-hash ___ \
	--control-plane --certificate-key ___
```

And another to add workers to the cluster:
```bash
kubeadm join $CLUSTER_NAME:6443 --token ___ \
	--discovery-token-ca-cert-hash ___
```

You can now also grab the kubectl admin configuration file from
`/etc/kubernetes/admin.conf`. Set `kubectl` up to use it, more information is at
the [kubernetes docs](https://kubernetes.io/docs/tasks/tools/).


### The rest of the control plane
Make a copy of the `../common-config.sh`, calling it, for example,
`control-plane-config.sh`. Add the command to join the control plane at the
bottom. Use this new file as the configuration for the new control plae nodes:

```bash
openstack server create k8s-control-plane-1 \
    --image "Debian 11" \
    --flavor $CONTROL_PLANE_FLAVOR \
    --port k8s-control-plane-1 \
    --security-group k8s-secgroup \
    --key-name $KEYNAME \
    --user-data control-plane-config.sh
openstack server create k8s-control-plane-2 \
    --image "Debian 11" \
    --flavor $CONTROL_PLANE_FLAVOR \
    --port k8s-control-plane-2 \
    --security-group k8s-secgroup \
    --key-name $KEYNAME \
    --user-data control-plane-config.sh
```

It will take a few minutes for the machines to become available. You will see
them come up with:

```bash
kubectl get nodes
```

The output should look something like this:
```
NAME                  STATUS   ROLES           AGE     VERSION
k8s-control-plane-0   Ready    control-plane   14m     v1.26.6
k8s-control-plane-1   Ready    control-plane   4m9s    v1.26.6
k8s-control-plane-2   Ready    control-plane   3m17s   v1.26.6
```

### Adding worker nodes
Now it is time to add some worker nodes. Make a second copy of
`../common-config.sh`, calling it, for example, `worker-config.sh`. Add the
command to join worker nodes to the cluster at the bottom. Use this new file
as the user data for the worker nodes:

```bash
openstack server create k8s-worker \
    --image "Debian 11" \
    --flavor $WORKER_FLAVOR \
    --network k8s-net \
    --security-group k8s-secgroup \
    --key-name $KEYNAME \
    --user-data worker-config.sh \
    --min $NUM_WORKERS \
    --max $NUM_WORKERS
```

Once again it will take a few minutes for the machines to come up and appear in
the cluster. You will be able to see them with:
```bash
kubectl get nodes
```

The output should look something like this:
```
NAME                  STATUS   ROLES           AGE     VERSION
k8s-control-plane-0   Ready    control-plane   32m   v1.26.6
k8s-control-plane-1   Ready    control-plane   21m   v1.26.6
k8s-control-plane-2   Ready    control-plane   20m   v1.26.6
k8s-worker-1          Ready    <none>          22s   v1.26.6
k8s-worker-2          Ready    <none>          23s   v1.26.6
k8s-worker-3          Ready    <none>          19s   v1.26.6
```

# Configuring the kubernetes
## Container networking
We have tested using an overlay network (VXLAN). If you have high network
demands you may want to look into a more performant solution:
```bash
curl https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico-vxlan.yaml \
    | sed 's/CrossSubnet/Always/' \
    | kubectl apply -f -
```

### Plug Kubernetes into OpenStack
You can use the OpenStack cloud provider
[integration](https://github.com/kubernetes/cloud-provider-openstack) to
provision persistent storage and load balancers for external access to your
kubernetes. You will need to create an openstack application credential for
kubernetes:
```bash
openstack application credential create k8s-cloud-provider
```
You will also need the subnet your workers reside on:
```bash
openstack subnet show k8s-subnet -f value -c id
```
and the identity of the external network you are using:
```bash
openstack network show $EXTERNAL_NETWORK -f value -c id
```

Fill the application credentials and network IDs into the provided `cloud.conf`,
the make it avaialble on your kubernetes as a secret:
```bash
kubectl create secret -n kube-system generic cloud-config --from-file=cloud.conf
```

We use the release of the
[cloud-provider-openstack](https://github.com/kubernetes/cloud-provider-openstack/)
corresponding to the kubernetes version:
```bash
git clone \
    --single-branch \
    --no-tags \
    --depth 1 \
    -b "release-$KUBERNETES_VERSION" \
    https://github.com/kubernetes/cloud-provider-openstack.git
rm cloud-provider-openstack/manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml

kubectl apply -f cloud-provider-openstack/manifests/controller-manager/cloud-controller-manager-roles.yaml
kubectl apply -f cloud-provider-openstack/manifests/controller-manager/cloud-controller-manager-role-bindings.yaml
kubectl apply -f cloud-provider-openstack/manifests/controller-manager/openstack-cloud-controller-manager-ds.yaml
kubectl apply -f cloud-provider-openstack/manifests/cinder-csi-plugin
kubectl apply -f cinder.yaml
```
